package andrey1993samaraGmailCom.actions;

import org.openqa.selenium.WebDriver;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScreenshotAction implements IAction {
    private SimpleDateFormat formatForDateNow = new SimpleDateFormat("yyyy.MM.dd h.m.s");

    @Override
    public void perform(WebDriver driver) {
        Date dateNow = new Date();
        BufferedImage image = null;
        try {
            Thread.sleep(5000); // чтобы успела прогрузиться траница
            image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            ImageIO.write(image, "png", new File(".//src//test//java//Screenshot//"+ formatForDateNow.format(dateNow) + ".png"));

        } catch (IOException | AWTException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
