package andrey1993samaraGmailCom.actions;

import org.openqa.selenium.WebDriver;

public class OpenUrlAction implements IAction {
    protected String url;

    public OpenUrlAction(String url) {
        this.url = url;
    }

    @Override
    public void perform(WebDriver driver) {
        driver.get(url);
    }
}
