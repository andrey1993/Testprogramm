package andrey1993samaraGmailCom.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ClickAction implements IAction{
    protected String locator;

    public ClickAction(String locator) {
        this.locator = locator;
    }

    @Override
    public void perform(WebDriver driver) {
        driver.findElement((By.xpath(locator))).click();
    }
}
