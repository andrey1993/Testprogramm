package andrey1993samaraGmailCom.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckElementVisibleAction implements IAction {
    protected String locator;

    public CheckElementVisibleAction(String locator) {
        this.locator = locator;
    }

    @Override
    public void perform(WebDriver driver) {
        try {
            driver.findElement((By.xpath(locator)));
            System.out.println("Элемент с локатором " + locator + " присутствует на странице");
        } catch (Exception e) {
            System.out.println("Элемента с локатором " + locator + " нет на странице");
        }
    }
}
