package andrey1993samaraGmailCom.actions;

import org.openqa.selenium.WebDriver;

public interface IAction {
    void perform(WebDriver driver);
}
