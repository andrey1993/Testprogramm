package andrey1993samaraGmailCom.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SetValueAction implements IAction {
    protected String locator;

    public SetValueAction(String locator) {
        this.locator = locator;
    }

    @Override
    public void perform(WebDriver driver) {
        int index = locator.indexOf("|");
        String loc = locator.substring(0, index).trim();
        String str = locator.substring(index + 1).trim();
        driver.findElement((By.xpath(loc))).sendKeys(str);
    }
}
