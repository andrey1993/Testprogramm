package andrey1993samaraGmailCom;

import andrey1993samaraGmailCom.readers.JsonReader;
import andrey1993samaraGmailCom.readers.XlsReader;
import andrey1993samaraGmailCom.readers.XlsxReader;
import andrey1993samaraGmailCom.readers.XmlReader;

import java.util.List;

public class SelectionReader {

    public static List<ActionParams> read(String filename) throws Exception {
        String str = filename.toLowerCase();
        if (str.endsWith(".xml")) return XmlReader.read(filename);
        if (str.endsWith(".json")) return JsonReader.read(filename);
        if (str.endsWith(".xlsx")) return XlsxReader.read(filename);
        if (str.endsWith(".xls")) return XlsReader.read(filename);
        throw new Exception("Unknown file format");
    }
}
