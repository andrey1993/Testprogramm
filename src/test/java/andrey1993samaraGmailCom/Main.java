package andrey1993samaraGmailCom;

import andrey1993samaraGmailCom.actions.IAction;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Main {
    public static WebDriver driver;

    public static void initDriver() {
        System.setProperty("webdriver.chrome.driver", ".//src//test//java//driver//chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
    }

    public static void closeDriver() {
        driver.quit();
    }

    public static void main(String[] args) throws IOException {
        initDriver();
        try {
            List<ActionParams> actionParamsList = SelectionReader.read(".//src//test//java//Json//file3.Json");

            List<IAction> actions = new ArrayList<>();

            for (ActionParams actionParams : actionParamsList) {
                actions.add(ActionsBuilder.build(actionParams));
            }
            for (IAction action : actions) {
                action.perform(driver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        closeDriver();
    }
}
