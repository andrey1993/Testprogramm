package andrey1993samaraGmailCom.readers;


import andrey1993samaraGmailCom.ActionParams;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class XlsxReader {

    public static ArrayList<ActionParams> read(String filename) throws IOException {
        ArrayList<ActionParams> actionParams = new ArrayList<>();
        String result = "";

        try (FileInputStream in = new FileInputStream(filename)) {
            XSSFWorkbook wb = new XSSFWorkbook(in);
            Sheet sheet = wb.getSheetAt(0);
            Iterator rowIter = sheet.rowIterator();
            while (rowIter.hasNext()) {
                XSSFRow row = (XSSFRow) rowIter.next();
                Iterator cellIter = row.cellIterator();
                while (cellIter.hasNext()) {
                    XSSFCell cell = (XSSFCell) cellIter.next();
                    result += cell + " ~ ";
                }
                result = result.substring(0, result.length() - 3);
                if (!result.equals("Actions ~ Params")) {
                    actionParams.add(new ActionParams(result.split("~")[0].trim(), result.split("~")[1].trim()));
                }
                result = "";
            }
            return actionParams;
        }
    }
}

