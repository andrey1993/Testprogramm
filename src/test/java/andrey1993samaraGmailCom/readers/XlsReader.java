package andrey1993samaraGmailCom.readers;

import andrey1993samaraGmailCom.ActionParams;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class XlsReader {

    public static ArrayList<ActionParams> read(String filename) throws IOException {
        ArrayList<ActionParams> actionParams = new ArrayList<>();
        String result = "";

        try (FileInputStream in = new FileInputStream(filename)) {
            HSSFWorkbook wb = new HSSFWorkbook(in);
            HSSFSheet sheet = wb.getSheetAt(0);
            Iterator rowIter = sheet.rowIterator();
            while (rowIter.hasNext()) {
                HSSFRow row = (HSSFRow) rowIter.next();
                Iterator cellIter = row.cellIterator();
                while (cellIter.hasNext()) {
                    HSSFCell cell = (HSSFCell) cellIter.next();
                    result += cell + " ~ ";
                }
                result = result.substring(0, result.length() - 3);
                if (!result.equals("Actions ~ Params")) {
                    actionParams.add(new ActionParams(result.split("~")[0].trim(), result.split("~")[1].trim()));
                }
                result = "";
            }
            return actionParams;
        }
    }
}
