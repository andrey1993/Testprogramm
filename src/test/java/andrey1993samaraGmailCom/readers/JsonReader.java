package andrey1993samaraGmailCom.readers;

import andrey1993samaraGmailCom.ActionParams;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.FileReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class JsonReader {
    private static Gson gson = new Gson();

    public static ArrayList<ActionParams> read(String filename) throws Exception {
        try (Reader reader = new FileReader(filename)) {
            Type type = new TypeToken<List<ActionParams>>() {
            }.getType();
            return gson.fromJson(reader, type);
        }
    }
}