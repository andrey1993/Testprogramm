package andrey1993samaraGmailCom.readers;

import andrey1993samaraGmailCom.ActionParams;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.ArrayList;

public class XmlReader {

    public static ArrayList<ActionParams> read(String filename) throws Exception {
        ArrayList<ActionParams> actionParamsList = new ArrayList<>();
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = documentBuilder.parse(filename);
        NodeList actionParamsElements = document.getDocumentElement().getElementsByTagName("actionParams");
        for (int i = 0; i < actionParamsElements.getLength(); i++) {
            Node actionParams = actionParamsElements.item(i);
            NamedNodeMap attributes = actionParams.getAttributes();
            actionParamsList.add(new ActionParams(attributes.getNamedItem("action").getNodeValue().trim(), attributes.getNamedItem("params").getNodeValue()));
        }
        return actionParamsList;
    }
}