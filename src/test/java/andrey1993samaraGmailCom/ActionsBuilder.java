package andrey1993samaraGmailCom;

import andrey1993samaraGmailCom.actions.*;

public class ActionsBuilder {

    public static IAction build(ActionParams actionParams) throws Exception {
        switch (actionParams.action.trim()) {
            case "openUrl":
                return new OpenUrlAction(actionParams.params);
            case "Click":
                return new ClickAction(actionParams.params);
            case "setValue":
                return new SetValueAction(actionParams.params);
            case "Screenshot":
                return new ScreenshotAction();
            case "checkElementVisible":
                return new CheckElementVisibleAction(actionParams.params);
            default:
                throw new Exception("action not found - " + actionParams.action);
        }
    }
}
